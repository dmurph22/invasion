#!/usr/bin/python

import os
import pygame

from pygame.locals import *
from game_objects.hero import Hero
from game_objects.minion import Minion
from game_objects.bomber import Bomber
from game_objects.platform import Platform
from game_objects.spritesheet import Spritesheet

SCREEN_SIZE = (750, 500)
GROUND_1_LOCATION = (1766, 2341, 192, 76)


def get_platform_sprites():
    ilmenskie_sheet_path = os.path.join('assets', 'index_ilmenskie.jpg')
    ilmenskie_sheet = Spritesheet(ilmenskie_sheet_path)

    ground_block = ilmenskie_sheet.get_image(GROUND_1_LOCATION)
    _, screen_height = SCREEN_SIZE
    ground_block_height = GROUND_1_LOCATION[3]
    ground_block_width = GROUND_1_LOCATION[2]
    y_offset = screen_height - ground_block_height
    ground_block_sprites = []
    for i in range(7):
        x_offset = i * ground_block_width
        ground_block_sprite = Platform(ground_block, x_offset, y_offset)
        ground_block_sprites.append(ground_block_sprite)

        if i == 2:
            for j in range(5):
                ground_block_sprite_stacked = Platform(ground_block, x_offset + 50 * j,
                                                       y_offset - ground_block_height * (j + 1))
                ground_block_sprites.append(ground_block_sprite_stacked)

    ground_block_sprite_group = pygame.sprite.Group()
    ground_block_sprite_group.add(ground_block_sprites)

    return ground_block_sprite_group


def get_hero_sprite():
    _, screen_height = SCREEN_SIZE

    hero_x_offset = 50
    hero_y_offset = 50

    hero = Hero(hero_x_offset,
                hero_y_offset)

    return hero


def main():
    # Initialise screen
    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE)
    pygame.display.set_caption('Invasion!')

    # Fill background
    background_image_path = os.path.join('assets', 'background0.png')
    background_image = pygame.image.load(background_image_path)
    scaled_background = pygame.transform.scale(background_image, SCREEN_SIZE)

    # Blit everything to the screen
    screen.blit(scaled_background, (0, 0))
    platform_sprites = get_platform_sprites()
    platform_sprites.draw(screen)

    character_sprites = pygame.sprite.Group()
    hero = get_hero_sprite()
    character_sprites.add(hero)
    character_sprites.draw(screen)

    minion_sprites = pygame.sprite.Group()
    minion_1 = Minion(500, 365)
    minion_sprites.add(minion_1)
    minion_sprites.draw(screen)

    screen_width, screen_height = SCREEN_SIZE
    bomber_sprites = pygame.sprite.Group()
    bomber_1 = Bomber(screen_width, 100)
    bomber_sprites.add(bomber_1)
    bomber_sprites.draw(screen)

    pygame.display.flip()

    player_xvel = 0
    player_yvel = 0

    # Event loop
    while 1:
        for event in pygame.event.get():
            if event.type == QUIT:
                return

            pressed_keys = pygame.key.get_pressed()

            hero_x_vel = 0
            if pressed_keys[pygame.K_RIGHT]:
                hero_x_vel += 10
            if pressed_keys[pygame.K_LEFT]:
                hero_x_vel -= 10
            if pressed_keys[pygame.K_UP]:
                hero.jump(hero.get_collisions(platform_sprites))
            if pressed_keys[pygame.K_SPACE]:
                hero.fire_laser()

            hero.set_x_vel(hero_x_vel)

            if event.type == pygame.KEYUP and not (pressed_keys[pygame.K_RIGHT] or pressed_keys[pygame.K_LEFT]):
                if event.key in (pygame.K_RIGHT, pygame.K_LEFT):
                    hero.set_x_vel(0)

        hero.update(platform_sprites)
        hero.rect.x = 300
        hero.rect.y = hero.yoffset

        for platform in platform_sprites:
            platform.update(hero.xoffset)

        for minion in minion_sprites:
            minion.update(hero.xoffset, hero.xvel)

        for bomber in bomber_sprites:
            bomber.update(hero.xoffset, hero.xvel)

            for bomb in bomber.bomb_sprites:
                bomb.update(hero.xvel)

        for laser in hero.laser_sprites:
            laser_x, _ = laser.rect.center
            if laser_x >= screen_width:
                laser.kill()
            else:
                laser.update(hero.xvel)

        laser_minion_collisions = pygame.sprite.groupcollide(hero.laser_sprites,
                                                             minion_sprites,
                                                             True,
                                                             True)

        hero_minion_collisions = pygame.sprite.groupcollide(character_sprites,
                                                            minion_sprites,
                                                            True,
                                                            False)
        if any(hero_minion_collisions):
            return

        screen.blit(scaled_background, (0, 0))

        platform_sprites.draw(screen)
        minion_sprites.draw(screen)
        bomber_sprites.draw(screen)
        for bomber in bomber_sprites:
            bomber.bomb_sprites.draw(screen)
        character_sprites.draw(screen)
        hero.laser_sprites.draw(screen)
        pygame.display.flip()


if __name__ == '__main__': main()
