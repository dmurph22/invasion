#!/usr/bin/python

import pygame


class Bomb(pygame.sprite.Sprite):
    def __init__(self, x, y, xvel):
        super().__init__()

        self.image = pygame.Surface([5, 5])
        self.image.fill((0, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.xvel = xvel
        self.yvel = 0

    def update(self, scroll_vel):
        self.rect.x += self.xvel - scroll_vel
        self.yvel += 0.3
        self.rect.y += self.yvel
