#!/usr/bin/python

import pygame


class Laser(pygame.sprite.Sprite):
    def __init__(self, image, x, y, direction):
        super().__init__()

        self.image = image
        self.rect = image.get_rect()
        self.original_x = x
        self.rect.x = x
        self.rect.y = y
        self.direction = direction

    def update(self, hero_xvel):
        self.rect.x += (18 * self.direction) - hero_xvel
