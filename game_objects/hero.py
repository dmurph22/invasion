#!/usr/bin/python

import os
import pygame

from math import floor
from .laser import Laser
from game_objects.spritesheet import Spritesheet

# Spritesheet location information
HERO_LOCATION_STAND = (19, 11, 43, 66)
HERO_LOCATION_WALKING = [
    (32, 179, 43, 60),
    (115, 178, 37, 60),
    (189, 178, 44, 60),
    (266, 177, 39, 60)
]
HERO_FIRING = [
    (11, 587, 40, 65),
    (58, 586, 49, 64),
    (115, 587, 58, 66),
    (176, 586, 53, 66)
]
LASER = (232, 608, 11, 11)
HERO_SPRITESHEET_COLORKEY = (0, 128, 0, 255)
HERO_HEIGHT = HERO_LOCATION_STAND[3]

# Load in images and set constants
XEON_SHEET_PATH = os.path.join('assets', 'xeonsheet.bmp')
XEON_SHEET = Spritesheet(XEON_SHEET_PATH, HERO_SPRITESHEET_COLORKEY)

STANDING_IMAGE = XEON_SHEET.get_image(HERO_LOCATION_STAND)
WALKING_IMAGES = [XEON_SHEET.get_image(location) for location in HERO_LOCATION_WALKING]
WALKING_SPEED_SCALE = .5
LASER_IMAGE = XEON_SHEET.get_image(LASER)
LASER_Y_OFFSET = 10


class Hero(pygame.sprite.Sprite):
    def __init__(self,
                 x,
                 y):
        super().__init__()

        self.set_image(STANDING_IMAGE, x, y)

        self.walking_image_num = 0

        self.xoffset = 0
        self.orient_left = False

        self.xvel = 0
        self.yvel = 0

        self.laser_sprites = pygame.sprite.Group()

    def set_image(self, image, x, y):
        '''Set image of hero'''
        self.image = image
        self.rect = image.get_rect()
        self.xoffset = x
        self.yoffset = y

    def set_x_vel(self, vel):
        '''Setter for x velocity'''
        self.xvel = vel

    def get_platform_on(self, platform_collisions):
        '''Determine which platform was landed on'''
        x_size = self.rect.width
        y_size = self.rect.height

        for platform in platform_collisions:
            hero_between_edges = self.rect.x + x_size > platform.rect.x and self.rect.x < platform.rect.x + platform.rect.width
            hero_above_bottom = self.yoffset + y_size < platform.rect.y + platform.rect.height // 2
            if self.yvel >= 0 and hero_between_edges and hero_above_bottom:
                return platform
        return None

    def jump(self, collisions):
        '''Jump if on a platform'''
        if self.get_platform_on(collisions) is not None:
            self.yvel = -20

    def get_collisions(self, group):
        '''Get collisions with given group'''
        return pygame.sprite.spritecollide(self, group, False)

    def adjust_to_platform_top(self, platform):
        '''Place hero on top of platform'''
        self.yoffset = platform.rect.y - self.rect.height + 1

    def set_next_walking_image(self, reverse=False):
        '''Get next walking image from walking image collection'''
        num_images = len(WALKING_IMAGES)
        scaled_num_images = int(num_images // WALKING_SPEED_SCALE)
        current_image_num = floor(self.walking_image_num * WALKING_SPEED_SCALE)
        current_image = WALKING_IMAGES[current_image_num]
        if reverse:
            current_image = pygame.transform.flip(current_image, True, False)
        self.set_image(current_image, self.xoffset, self.yoffset)
        self.walking_image_num = (self.walking_image_num + 1) % scaled_num_images

    def fire_laser(self):
        '''Fire laser weapon'''
        direction = -1 if self.orient_left else 1
        init_x, init_y = self.rect.x, self.rect.y + LASER_Y_OFFSET
        new_laser = Laser(LASER_IMAGE, init_x, init_y, direction)
        self.laser_sprites.add(new_laser)

    def update(self, platforms):
        '''Update state'''
        self.xoffset += self.xvel
        self.yoffset += self.yvel
        self.rect.y = self.yoffset

        on_platform = self.get_platform_on(self.get_collisions(platforms))

        self.orient_left = self.xvel < 0 or (self.orient_left and self.xvel <= 0)

        if self.xvel != 0 and on_platform is not None:
            self.set_next_walking_image(reverse=self.orient_left)
        else:
            self.walking_image_num = 0
            if self.orient_left:
                standing_image = pygame.transform.flip(STANDING_IMAGE, True, False)
            else:
                standing_image = STANDING_IMAGE
            self.set_image(standing_image, self.xoffset, self.yoffset)

        if on_platform is not None:
            if self.yvel >= 0:
                self.yvel = 0
            self.adjust_to_platform_top(on_platform)
        else:
            self.yvel += 2
