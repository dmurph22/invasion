import os
import pygame

from game_objects.spritesheet import Spritesheet
from game_objects.bomb import Bomb

BOMBER_LOCATION = (0, 64, 30, 30)
BOMBER_SHEET_COLORKEY = (0, 0, 0)
BOMBER_SHEET_PATH = os.path.join('assets', 'creatures.png')
BOMBER_SHEET = Spritesheet(BOMBER_SHEET_PATH, BOMBER_SHEET_COLORKEY)
BOMBER_IMAGE = BOMBER_SHEET.get_image(BOMBER_LOCATION)
BOMBER_UPDATE_SIZE = 3
UPDATES_BETWEEN_BOMBS = 60


class Bomber(pygame.sprite.Sprite):
    def __init__(self,
                 x,
                 y,
                 orient_left=True):
        super().__init__()

        self.original_x = x
        self.orient_left = orient_left

        self.image = BOMBER_IMAGE
        self.rect = self.image.get_rect()

        self.rect.x = x
        self.rect.y = y
        self.bomb_sprites = pygame.sprite.Group()
        self.updates_since_last_bomb = 0

    def update(self, xoffset, scroll_vel):
        relative_original_x_pos = self.original_x - xoffset
        relative_minion_offset = self.rect.x - relative_original_x_pos

        x_update = self.get_x_vel() - scroll_vel

        self.rect.x = relative_original_x_pos + relative_minion_offset + x_update

        self.updates_since_last_bomb += 1
        if self.updates_since_last_bomb >= UPDATES_BETWEEN_BOMBS:
            self.drop_bomb()
            self.updates_since_last_bomb = 0

    def get_x_vel(self):
        return -BOMBER_UPDATE_SIZE if self.orient_left else BOMBER_UPDATE_SIZE

    def drop_bomb(self):
        bomb = Bomb(self.rect.x, self.rect.y, self.get_x_vel())
        self.bomb_sprites.add(bomb)
