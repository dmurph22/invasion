import os
import pygame

from pygame.locals import *
from game_objects.spritesheet import Spritesheet
from math import fabs

MINION_LOCATION = (0,545,30,30)
MINION_SHEET_COLORKEY = (0,0,0)
MINION_SHEET_PATH = os.path.join('assets', 'creatures.png')
MINION_SHEET = Spritesheet(MINION_SHEET_PATH, MINION_SHEET_COLORKEY)
MINION_IMAGE = MINION_SHEET.get_image(MINION_LOCATION)
MINION_UPDATE_SIZE = 3

class Minion(pygame.sprite.Sprite):
    def __init__(self,
                 x,
                 y):
        super().__init__()

        self.original_x = x
        self.orient_left = True

        self.image = MINION_IMAGE
        self.rect = self.image.get_rect()

        self.rect.x = x
        self.rect.y = y

    def update(self, xoffset, scroll_vel):
        self.xoffset = xoffset

        relative_original_x_pos = self.original_x - xoffset
        relative_minion_offset = self.rect.x - relative_original_x_pos

        if relative_minion_offset >= 100:
            self.orient_left = True
        elif relative_minion_offset <= -100:
            self.orient_left = False

        x_update = (-MINION_UPDATE_SIZE if self.orient_left else MINION_UPDATE_SIZE) - scroll_vel

        self.rect.x = relative_original_x_pos + relative_minion_offset + x_update

