#!/usr/bin/python

import pygame


class Platform(pygame.sprite.Sprite):
    def __init__(self, image, x, y):
        super().__init__()

        self.image = image
        self.rect = image.get_rect()
        self.original_x = x
        self.rect.x = x
        self.rect.y = y

    def update(self, xoffset):
        self.rect.x = self.original_x - xoffset
