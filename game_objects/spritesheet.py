#!/usr/bin/python

import pygame


class Spritesheet:
    def __init__(self, filename, colorkey=None):
        self.sheet = pygame.image.load(filename)
        self.colorkey = colorkey

    def get_image(self, location):
        '''Returns a pygame Surface with the image at the given location drawn on it'''
        (_, _, width, height) = location
        image = pygame.Surface([width, height])
        image.set_colorkey(self.colorkey)
        image.blit(self.sheet, (0, 0), location)
        return image
